package br.com.eprecise.main.configs.openapi

import javax.ws.rs.ApplicationPath
import javax.ws.rs.core.Application
import org.eclipse.microprofile.openapi.annotations.OpenAPIDefinition
import org.eclipse.microprofile.openapi.annotations.info.Contact
import org.eclipse.microprofile.openapi.annotations.info.Info

@ApplicationPath("api/v1")
@OpenAPIDefinition(
        info =
                Info(
                        title = "Cidade -  Estado API",
                        description = "Api desenvolvido durante o processo selectivo",
                        version = "1.0",
                        contact =
                                Contact(
                                        name = "Faustino Manuel",
                                        url = "https://github.com/fmanuel98",
                                        email = "faugf320@gmail.om"
                                )
                )
)
class OpenApiConfiguration : Application()
