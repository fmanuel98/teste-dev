package br.com.eprecise.domain.entities
data class Cidade(val id: Long = 0, val nome: String, val estado: Estado)
