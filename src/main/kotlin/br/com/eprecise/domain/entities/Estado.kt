package br.com.eprecise.domain.entities
data class Estado(val id: Long = 0, val nome: String = "", val sigla: String = "") {
  constructor(id: Long) : this(id, "")
}
