package br.com.eprecise.domain.useases

import br.com.eprecise.domain.entities.Cidade

interface CadastrarCidade {
  fun cadastar(cidade: Cidade): Long
}

interface MudarNomeCidade {
  fun mudarNomeCidade(key: Long, cidade: Cidade): Unit
}

interface BuscarCidade<E> {
  fun listarCidades(numeroPagina: Int, totalElementos: Int): E
  fun buscarPorNome(nome: String, numeroPagina: Int, totalElementos: Int): E
  fun buscarPorEstado(estadoId: Long, numeroPagina: Int, totalElementos: Int): E
  fun countCidade(): Long
}
