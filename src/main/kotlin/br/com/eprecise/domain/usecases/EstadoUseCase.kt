package br.com.eprecise.domain.useases

import br.com.eprecise.domain.entities.Estado

interface CadastrarEstado {
  fun cadastar(estadoDomain: Estado): Long
}

interface MudarNomeOuSiglaEstado {
  fun mudarNomeOuSiglaEstado(key: Long, estadoDomain: Estado): Unit
}

interface BuscarEstado<E> {
  fun listarEstado(numeroPagina: Int, totalElementos: Int): E
  fun countEstado(): Long
}
