package br.com.eprecise.domain.errors

open class DomainException(message: String) : RuntimeException(message)

class EntityUnSaveException(message: String = "Falha ao salvar") : DomainException(message)

class EntityNotFoundException(message: String = "Item não encontrado") : DomainException(message)

class EntityAlreadyExistsException(message: String = "Ja existe item com este nome") :
    DomainException(message)
