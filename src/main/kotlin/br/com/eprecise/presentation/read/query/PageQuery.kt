package br.com.eprecise.presentation.read.queries

class PageQuery<T>(
    val contect: List<T>,
    val isVazio: Boolean,
    val totalElementos: Long,
    val numeroPagina: Int,
    val totalElementosPagina: Int
)
