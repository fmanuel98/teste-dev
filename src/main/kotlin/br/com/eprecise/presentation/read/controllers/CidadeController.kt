package br.com.eprecise.presentation.read.controllers

import br.com.eprecise.app.repositories.CidadeReposiotry
import br.com.eprecise.presentation.read.queries.CidadeQuery
import br.com.eprecise.presentation.read.queries.PageQuery
import javax.validation.Valid
import javax.validation.constraints.Size
import javax.ws.rs.*
import org.eclipse.microprofile.openapi.annotations.tags.Tag

@Tag(name = "Cidades")
@Path("cidades")
class CidadeController(val repository: CidadeReposiotry) {
  @GET
  fun listarCidades(
      @QueryParam("numeroPagina") @DefaultValue("1") numeroPagina: Int,
      @QueryParam("totalElementos") @DefaultValue("6") totalElementos: Int
  ): PageQuery<CidadeQuery> {
    val estadosPaginados = repository.listarCidades(numeroPagina, totalElementos)
    val estaddos = estadosPaginados.list().map { CidadeQuery(it.toModel()) }
    return PageQuery<CidadeQuery>(
        contect = estaddos,
        isVazio = estaddos.isEmpty(),
        totalElementos = estadosPaginados.count(),
        totalElementosPagina = estaddos.size,
        numeroPagina = numeroPagina
    )
  }
  @GET
  @Path("estado")
  fun listarCidadesPorEstado(
      @QueryParam("numeroPagina") @DefaultValue("1") numeroPagina: Int,
      @QueryParam("totalElementos") @DefaultValue("6") totalElementos: Int,
      @QueryParam("estadoId") estadoId: Long
  ): PageQuery<CidadeQuery> {
    val estadosPaginados = repository.buscarPorEstado(estadoId, numeroPagina, totalElementos)
    val estaddos = estadosPaginados.list().map { CidadeQuery(it.toModel()) }
    return PageQuery<CidadeQuery>(
        contect = estaddos,
        isVazio = estaddos.isEmpty(),
        totalElementos = estadosPaginados.count(),
        totalElementosPagina = estaddos.size,
        numeroPagina = numeroPagina
    )
  }
  @GET
  @Path("nome")
  fun listarCidadesPorNome(
      @QueryParam("numeroPagina") @DefaultValue("1") numeroPagina: Int,
      @QueryParam("totalElementos") @DefaultValue("6") totalElementos: Int,
      @Valid @Size(min = 3) @QueryParam("nome") nome: String
  ): PageQuery<CidadeQuery> {
    val estadosPaginados = repository.buscarPorNome(nome, numeroPagina, totalElementos)
    val estaddos = estadosPaginados.list().map { CidadeQuery(it.toModel()) }
    return PageQuery<CidadeQuery>(
        contect = estaddos,
        isVazio = estaddos.isEmpty(),
        totalElementos = estadosPaginados.count(),
        totalElementosPagina = estaddos.size,
        numeroPagina = numeroPagina
    )
  }

  @GET @Path("total-cidades") fun totalCidades(): Long = repository.countCidade()
}
