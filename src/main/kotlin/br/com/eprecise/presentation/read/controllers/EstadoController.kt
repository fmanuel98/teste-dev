package br.com.eprecise.presentation.read.controllers

import br.com.eprecise.app.repositories.EstadoReposiotry
import br.com.eprecise.presentation.read.queries.EstadoQuery
import br.com.eprecise.presentation.read.queries.PageQuery
import javax.ws.rs.*
import org.eclipse.microprofile.openapi.annotations.tags.Tag

@Tag(name = "Estados")
@Path("estados")
class EstadoController(val repository: EstadoReposiotry) {
  @GET
  fun listarEstados(
      @QueryParam("numeroPagina") @DefaultValue("1") numeroPagina: Int,
      @QueryParam("totalElementos") @DefaultValue("6") totalElementos: Int
  ): PageQuery<EstadoQuery> {
    val estadosPaginados = repository.listarEstado(numeroPagina, totalElementos)
    val estaddos = estadosPaginados.list().map { EstadoQuery(it.toModel()) }
    return PageQuery<EstadoQuery>(
        contect = estaddos,
        isVazio = estaddos.isEmpty(),
        totalElementos = estadosPaginados.count(),
        totalElementosPagina = estaddos.size,
        numeroPagina = numeroPagina
    )
  }

  @GET @Path("total-estados") fun totalEstado(): Long = repository.countEstado()
}
