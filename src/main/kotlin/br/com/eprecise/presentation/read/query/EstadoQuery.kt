package br.com.eprecise.presentation.read.queries

import br.com.eprecise.domain.entities.Estado

class EstadoQuery(val id: Long, val nome: String, val sigla: String) {
  constructor(domain: Estado) : this(domain.id, domain.nome, domain.sigla)
}
