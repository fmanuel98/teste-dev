package br.com.eprecise.presentation.read.queries

import br.com.eprecise.domain.entities.Cidade

class CidadeQuery(val id: Long, val nome: String) {
  constructor(domain: Cidade) : this(domain.id, domain.nome)
}
