package br.com.eprecise.presentation.errors

import java.time.OffsetDateTime
import org.eclipse.microprofile.openapi.annotations.media.Schema

@Schema(name = "ErrorResponse")
class ErrorResponse(
    @Schema(required = true, example = "400") val status: Int,
    @Schema(required = true, example = "2022-03-01T18:09:02.70844Z")
    val timestamp: OffsetDateTime = OffsetDateTime.now(),
    @Schema(example = "Dados inválidos") val title: String = "Dados inválidos",
    @Schema(example = "Um ou mais campos estão inválidos", name = "Fields")
    val detail: String? = null,
    val fields: List<Field>? = null,
) {
  @Schema(example = " Lista de objetos ou campos que geraram o erro (opcional)")
  companion object {
    @Schema(name = "Field")
    class Field(
        @Schema(example = "nome") val field: String,
        @Schema(example = "O nome é obrigatório") val mensagem: String
    )
  }
}
