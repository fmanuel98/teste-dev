package br.com.eprecise.presentation.errors

import br.com.eprecise.domain.errors.EntityAlreadyExistsException
import br.com.eprecise.domain.errors.EntityNotFoundException
import javax.transaction.RollbackException
import javax.ws.rs.core.Response
import javax.ws.rs.core.Response.Status
import javax.ws.rs.ext.ExceptionMapper
import javax.ws.rs.ext.Provider

@Provider
class ExceptionHandler : ExceptionMapper<Exception> {
  val MSG_ERRO_GENERICA_USUARIO_FINAL =
      "Ocorreu um erro interno inesperado no sistema. Tente novamente e se o problema persistir, entre em contato com o administrador do sistema."
  override fun toResponse(response: Exception): Response {
    val code =
        when (response) {
          is EntityAlreadyExistsException -> Status.CONFLICT.statusCode
          is RollbackException -> Status.CONFLICT.statusCode
          is EntityNotFoundException -> Status.NOT_FOUND.statusCode
          else -> Status.BAD_REQUEST.statusCode
        }
    val error = ErrorResponse(status = code, detail = response.message)
    return Response.status(error.status).entity(error).build()
  }
}
