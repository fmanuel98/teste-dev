package br.com.eprecise.presentation.write.controllers

import br.com.eprecise.app.services.EstadoService
import br.com.eprecise.presentation.write.commands.CreateEstadoCommand
import javax.validation.Valid
import javax.ws.rs.*
import javax.ws.rs.core.Context
import javax.ws.rs.core.Response
import javax.ws.rs.core.UriInfo
import org.eclipse.microprofile.openapi.annotations.tags.Tag

@Tag(name = "Estados")
@Path("estados")
class EstadoController(val service: EstadoService) {
  @POST
  fun CadastrarEstado(@Valid input: CreateEstadoCommand, @Context i: UriInfo) =
      input.let { it.toModel() }.run { service.cadastar(this) }.let {
        Response.created(i.absolutePathBuilder.path(it.toString()).build()).build()
      }

  @PUT
  @Path("{estadoId}")
  fun actualizarEstado(@Valid input: CreateEstadoCommand, @PathParam("estadoId") estadoId: Long) =
      service.run { this.mudarNomeOuSiglaEstado(estadoId, input.toModel()) }.let {
        Response.noContent().build()
      }
  @DELETE
  @Path("{estadoId}")
  fun removerEstado(@PathParam("estadoId") estadoId: Long) =
      service.run { this.removerEstado(estadoId) }.let { Response.noContent().build() }
}
