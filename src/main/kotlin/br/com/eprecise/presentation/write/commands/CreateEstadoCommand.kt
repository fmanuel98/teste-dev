package br.com.eprecise.presentation.write.commands

import br.com.eprecise.domain.entities.Estado
import javax.validation.constraints.NotBlank

class CreateEstadoCommand(@field:NotBlank val nome: String, @field:NotBlank val sigla: String) {
  fun toModel() = Estado(nome = nome, sigla = sigla)
}
