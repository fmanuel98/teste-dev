package br.com.eprecise.presentation.write.controllers

import br.com.eprecise.app.services.CidadeService
import br.com.eprecise.presentation.write.commands.CreateCidadeCommand
import javax.validation.Valid
import javax.ws.rs.*
import javax.ws.rs.core.Context
import javax.ws.rs.core.Response
import javax.ws.rs.core.UriInfo
import org.eclipse.microprofile.openapi.annotations.tags.Tag

@Tag(name = "Cidades")
@Path("cidades")
class CidadeController(val service: CidadeService) {
  @POST
  fun cadastrarCidade(@Valid input: CreateCidadeCommand, @Context i: UriInfo) =
      input.let { it.toModel() }.run { service.cadastar(this) }.let {
        Response.created(i.absolutePathBuilder.path(it.toString()).build()).build()
      }

  @PUT
  @Path("{cidadeId}")
  fun actualizarCidade(@Valid input: CreateCidadeCommand, @PathParam("cidadeId") cidadeId: Long) =
      service.run { this.mudarNomeCidade(cidadeId, input.toModel()) }.let {
        Response.noContent().build()
      }

  @DELETE
  @Path("{cidadeId}")
  fun removerCidade(@PathParam("cidadeId") cidadeId: Long) =
      service.run { this.removerCidade(cidadeId) }.let { Response.noContent().build() }
}
