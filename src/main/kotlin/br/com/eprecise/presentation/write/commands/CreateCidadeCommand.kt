package br.com.eprecise.presentation.write.commands

import br.com.eprecise.domain.entities.Cidade
import br.com.eprecise.domain.entities.Estado
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull

class CreateCidadeCommand(@field:NotBlank val nome: String, @field:NotNull val estadoId: Long) {
  fun toModel() = Cidade(nome = nome, estado = Estado(estadoId))
}
