package br.com.eprecise;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Main {

  public static void main(String[] args) {
    var formatter = DateTimeFormatter.ofPattern("yyyy.MM.dd.HH.mm.ss");
    var dateTime = LocalDateTime.now();
    String formattedDateTime = dateTime.format(formatter);
    System.out.println("V" + formattedDateTime + "__");
  }
}
