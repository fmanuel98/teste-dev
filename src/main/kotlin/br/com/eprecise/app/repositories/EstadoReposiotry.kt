package br.com.eprecise.app.repositories

import br.com.eprecise.app.models.EstadoModel
import br.com.eprecise.domain.entities.Estado
import br.com.eprecise.domain.errors.EntityAlreadyExistsException
import br.com.eprecise.domain.useases.BuscarEstado
import br.com.eprecise.domain.useases.CadastrarEstado
import io.quarkus.hibernate.orm.panache.kotlin.PanacheQuery
import io.quarkus.hibernate.orm.panache.kotlin.PanacheRepository
import javax.enterprise.context.ApplicationScoped
import javax.persistence.*
import javax.persistence.PersistenceException

@ApplicationScoped
class EstadoReposiotry :
    PanacheRepository<EstadoModel>, CadastrarEstado, BuscarEstado<PanacheQuery<EstadoModel>> {
  override fun cadastar(estadoDomain: Estado): Long {
    return try {
      val model = EstadoModel(estadoDomain)
      this.persist(model)
      model.toModel().id
    } catch (e: PersistenceException) {
      throw EntityAlreadyExistsException("Ja existe um estado cadastrado com este nome ou sigla")
    }
  }
  override fun listarEstado(numeroPagina: Int, totalElementos: Int): PanacheQuery<EstadoModel> {
    return this.findAll().page(numeroPagina - 1, totalElementos)
  }
  override fun countEstado() = this.count()
}
