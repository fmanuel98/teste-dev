package br.com.eprecise.app.repositories

import br.com.eprecise.app.models.CidadeModel
import br.com.eprecise.domain.entities.Cidade
import br.com.eprecise.domain.errors.EntityAlreadyExistsException
import br.com.eprecise.domain.useases.BuscarCidade
import br.com.eprecise.domain.useases.CadastrarCidade
import io.quarkus.hibernate.orm.panache.kotlin.PanacheQuery
import io.quarkus.hibernate.orm.panache.kotlin.PanacheRepository
import javax.enterprise.context.ApplicationScoped
import javax.persistence.*

@ApplicationScoped
class CidadeReposiotry :
    PanacheRepository<CidadeModel>, CadastrarCidade, BuscarCidade<PanacheQuery<CidadeModel>> {
  override fun cadastar(cidade: Cidade): Long {
    return try {
      val model = CidadeModel(cidade)
      this.persist(model)
      model.toModel().id
    } catch (e: PersistenceException) {
      throw EntityAlreadyExistsException("Ja existe uma cidade cadastrada com este nome")
    }
  }
  override fun listarCidades(numeroPagina: Int, totalElementos: Int): PanacheQuery<CidadeModel> {
    return this.findAll().page(numeroPagina - 1, totalElementos)
  }
  override fun buscarPorNome(
      nome: String,
      numeroPagina: Int,
      totalElementos: Int
  ): PanacheQuery<CidadeModel> {
    return this.find("nome like ?1", "%$nome%").page(numeroPagina - 1, totalElementos)
  }

  override fun buscarPorEstado(
      estadoId: Long,
      numeroPagina: Int,
      totalElementos: Int
  ): PanacheQuery<CidadeModel> {
    return this.find("estado_id", estadoId).page(numeroPagina - 1, totalElementos)
  }
  override fun countCidade() = this.count()
}
