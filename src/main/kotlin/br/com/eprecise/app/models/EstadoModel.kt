package br.com.eprecise.app.models

import br.com.eprecise.domain.entities.Estado
import javax.persistence.*

@Entity
@Table(name = "estados")
data class EstadoModel(
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Long = 0,
    @Column(nullable = false, unique = true) var nome: String = "",
    @Column(nullable = false, unique = true) var sigla: String = ""
) {

  @OneToMany(orphanRemoval = true, fetch = FetchType.LAZY, mappedBy = "estado")
  lateinit var estados: List<CidadeModel>
  constructor(domain: Estado) : this(domain.id, domain.nome, domain.sigla)
  constructor() : this(0L)
  constructor(id: Long) : this(id, "")
  fun toModel() = Estado(id = id, nome = nome, sigla = sigla)
}
