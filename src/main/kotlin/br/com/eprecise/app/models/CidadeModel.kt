package br.com.eprecise.app.models

import br.com.eprecise.domain.entities.Cidade
import javax.persistence.*

@Entity
@Table(name = "cidades")
data class CidadeModel(
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Long = 0,
    @Column(nullable = false,unique = true) var nome: String = "",
    @ManyToOne(optional = false) var estado: EstadoModel
) {
  constructor() : this(id = 0L, estado = EstadoModel())
  constructor(domain: Cidade) : this(domain.id, domain.nome, EstadoModel(domain.estado))
  fun toModel() = Cidade(id, nome, estado.toModel())
}
