package br.com.eprecise.app.services

import br.com.eprecise.app.repositories.EstadoReposiotry
import br.com.eprecise.domain.entities.Estado
import br.com.eprecise.domain.errors.EntityNotFoundException
import br.com.eprecise.domain.useases.CadastrarEstado
import br.com.eprecise.domain.useases.MudarNomeOuSiglaEstado
import javax.enterprise.context.ApplicationScoped
import javax.transaction.Transactional

@ApplicationScoped
class EstadoService(val repository: EstadoReposiotry) : CadastrarEstado, MudarNomeOuSiglaEstado {
  @Transactional override fun cadastar(estadoDomain: Estado) = repository.cadastar(estadoDomain)

  @Transactional
  override fun mudarNomeOuSiglaEstado(key: Long, estadoDomain: Estado): Unit {
    val estadoApp = findById(key)
    estadoApp.apply {
      this.nome = estadoDomain.nome
      this.sigla = estadoDomain.sigla
    }
    estadoApp.toModel()
  }

  fun findById(key: Long) =
      repository.findById(key) ?: throw EntityNotFoundException("estado $key nao encontrado")
  @Transactional fun removerEstado(key: Long) = findById(key).run { repository.delete(this) }
}
