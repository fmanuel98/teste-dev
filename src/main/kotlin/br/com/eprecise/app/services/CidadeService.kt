package br.com.eprecise.app.services

import br.com.eprecise.app.models.CidadeModel
import br.com.eprecise.app.models.EstadoModel
import br.com.eprecise.app.repositories.CidadeReposiotry
import br.com.eprecise.domain.entities.Cidade
import br.com.eprecise.domain.errors.EntityNotFoundException
import br.com.eprecise.domain.useases.CadastrarCidade
import br.com.eprecise.domain.useases.MudarNomeCidade
import javax.enterprise.context.ApplicationScoped
import javax.transaction.Transactional

@ApplicationScoped
class CidadeService(val repository: CidadeReposiotry) : CadastrarCidade, MudarNomeCidade {
  @Transactional override fun cadastar(cidade: Cidade) = repository.cadastar(cidade)

  @Transactional
  override fun mudarNomeCidade(key: Long, cidade: Cidade): Unit {
    val cidadeApp = findById(key)
    cidadeApp.apply {
      this.nome = cidade.nome
      this.estado = EstadoModel(cidade.estado.id)
    }
    cidadeApp.toModel()
  }

  fun findById(key: Long): CidadeModel =
      repository.findById(key) ?: throw EntityNotFoundException("cidade $key nao encontrada")
  @Transactional fun removerCidade(key: Long) = findById(key).run { repository.delete(this) }
}
