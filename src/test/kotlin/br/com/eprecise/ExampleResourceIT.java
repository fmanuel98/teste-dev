package br.com.eprecise;

import br.com.eprecise.presentation.write.commands.CreateEstadoCommand;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;

@QuarkusTest
public class ExampleResourceIT {


  @Test()
  @DisplayName("Simple Resource Test Example")
  public void criarEstado() {
    var estado = new CreateEstadoCommand("Luanda", "LA");
    given().body(estado).contentType(ContentType.JSON)
        .when().post("/api/v1/estados")
        .then()
        .statusCode(201);
  }

}
